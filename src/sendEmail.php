<?php

function sendEmail($email, $from, $title, $subject, $data) {

$host = $_SERVER['HTTP_HOST'];
$ref = $_SERVER['HTTP_REFERER'];

$body = '';
$body .= $title .'<br/>';

foreach($data as $field => $value) {
  $body .= $value['title'] .': '.$value['value'] .'<br/>';
}

$body .= $ref;

mb_internal_encoding("UTF-8");
$subj = mb_encode_mimeheader($subject, "UTF-8", "Q");

$un = strtoupper(uniqid(time()));
$headers = "From: $from\n";
$headers .= "To: $email\n";
$headers .= "Reply-To: $from\n";
$headers .= "Mime-Version: 1.0\n";
$headers .= "Content-Type:multipart/mixed;";
$headers .= "boundary=\"----------".$un."\"\n\n";
$text = "------------".$un."\nContent-Type:text/html; charset=\"utf-8\"\n";
$text .= "Content-Transfer-Encoding: 8bit\n\n$body\n\n";

$mail = mail("$email", "$subj", $text, $headers);

$response;

if($mail) {
  $response = '';
} else {
  $response = 'ERROR';
}

echo  json_encode($response);

}

?>