var forms = (function() {
  var forms = [];

  function Form(form, actionUrl) {

    this.errorDuration = 3000;

    this.form = form;
    this.actionUrl = actionUrl;
    this.submited = false;
    this.name = this.form.attributes.name.nodeValue;
    this.antiSpam = this.form.querySelector('[name = "as"]');
    this.submitBtn = this.form.querySelector('[type = "submit"]');
    this.loadingIcon = createLoadingIcon();

    var self = this;

    this.form.addEventListener('submit', function(e) { self.submit(e) });
    this.form.addEventListener('change', function(e) { self.validate(e) });

    this.submitBtn.addEventListener('mouseenter', function() { self.notRobot() });
    this.submitBtn.addEventListener('mouseover', function() { self.notRobot() });
  }

  Form.prototype = {
    'validate': function(e) {
      if (debug) { console.info('validation'); };
      e.preventDefault();

      var data = new FormData(this.form);
      data.append('formName', this.name);
      data.append('check', true);

      var self = this;
      sendData(this.actionUrl, data, function(text) { handleResponse.call(self, text) });

      function handleResponse(text) {
        this.clearErrors();

        if (text !== '') {
          this.showError(text, true);
        }
      }
    },
    'submit': function(e) {
      if (debug) { console.info('submit'); };
      e.preventDefault();

      if (this.submited) return;

      var data = new FormData(this.form);
      data.append('formName', this.name);
      data.append('submit', true);

      var self = this;
      this.startLoading();
      this.disableInput();

      sendData(this.actionUrl, data, function(text) { handleResponse.call(self, text) });

      function handleResponse(text) {
        if (text !== '') {
          this.showError(text);
        } else {
          this.submited = true;
          this.showResponse('<p>Спасибо за вашу заявку!</p>');
        }
        this.finishLoading();
        this.enableInput();
      }
    },
    'notRobot': function() {
      this.antiSpam.value = '';
      this.antiSpam.defaultValue = '';
    },
    'startLoading': function() {
      if (debug) { console.info('start loading'); };
      this.form.appendChild(this.loadingIcon);
    },
    'finishLoading': function() {
      if (debug) { console.info('finish loading'); };
      this.loadingIcon.parentNode.removeChild(this.loadingIcon);
    },
    'showResponse': function(text) {
      if (debug) { console.info(text); };
      var childs = this.form.children;

      for (var i = 0; i < childs.length; i++) {
        childs[i].classList.add('smf__zero_height');
      }
      var resp = document.createElement('div');
      resp.className = 'smf__response';
      resp.innerHTML = text;
      this.form.appendChild(resp);
    },
    'showError': function(error, force) {
      if (debug) { console.error(error); };
      this.errorCount = 0;
      var errorWidth = 270;

      function getErrorPosition(errorEl) {
        if (parent.clientWidth + errorWidth >= document.body.clientWidth) {
          errorEl.style.top = parent.offsetTop + parent.clientHeight + 5 + 'px';
          errorEl.style.left = 0;
          errorEl.style.width = '100%';
          errorEl.classList.remove('smf__error_right');
        } else {
          errorEl.style.top = parent.offsetTop + 'px';
          errorEl.style.left = parent.clientWidth + 5 + 'px';
          errorEl.style.width = errorWidth + 'px';
          errorEl.classList.add('smf__error_right');
        }
      }

      for (var name in error) {
        this.errorCount++;
        var errorEl = this.form.querySelector('div[data-error=' + name + ']');
        var parent = this.form.querySelector('[name=' + name + ']');

        if (parent.value === '' && force) continue;
        parent.classList.add('smf__error_field');

        if (errorEl) {
          errorEl.children[0].innerHTML = error[name];
          errorEl.style.display = 'block';
          getErrorPosition(errorEl);
          hideWithAnim(errorEl, 'smf__hide', this.errorDuration, 300);
        } else {
          errorEl = document.createElement('div');
          errorEl.className = 'smf__error';
          errorEl.setAttribute('data-error', name);
          getErrorPosition(errorEl);
          errorEl.style.display = 'block';
          errorEl.style.zIndex = this.errorCount;
          this.form.appendChild(errorEl);
          errorText = document.createElement('div');
          errorText.innerHTML = error[name];
          errorText.className = 'smf__error_text';
          errorEl.appendChild(errorText);          
          hideWithAnim(errorEl, 'smf__hide', this.errorDuration, 300);
        }
      }
    },
    'clearErrors': function() {
      var childs = this.form.children;

      for (var i = 0; i < childs.length; i++) {
        if (childs[i].nodeName === 'INPUT' || childs[i].nodeName === 'TEXTAREA') {
          childs[i].classList.remove('smf__error_field');
        }
      }
    },
    'disableInput': function() {
      var childs = this.form.children;

      for (var i = 0; i < childs.length; i++) {
        childs[i].disabled = true;
      }
    },
    'enableInput': function() {
      var childs = this.form.children;

      for (var i = 0; i < childs.length; i++) {
        childs[i].disabled = false;
      }
    }
  }

  function initForms(actionUrl) {
    var allForms = document.querySelectorAll('form[action="' + actionUrl + '"]');
    for (var i = 0; i < allForms.length; i++) {
      forms[i] = new Form(allForms[i], actionUrl);
    }
    return forms;
  }

  return {
    init: initForms,
    forms: forms
  }
})();

function createLoadingIcon() {
  var loading = document.createElement("div");
  var circle = document.createElement("div");
  var circleMask = document.createElement("div");
  loading.className = 'smf__loading';
  circle.className = 'smf__loading__circle';
  circleMask.className = 'smf__loading__circle_mask';
  circle.appendChild(circleMask);
  loading.appendChild(circle);

  return loading;
}

function hideWithAnim(el, className, errorDuration, animDuration) {
  setTimeout(function() { el.classList.toggle(className) }, errorDuration);
  setTimeout(function() {
    el.style.display = 'none';
    el.classList.toggle(className)
  }, errorDuration + animDuration);
}

function sendData(url, data, done, onStateChange) {
  var req = new XMLHttpRequest();
  req.open("POST", url, true);
  req.onprogress = function() {
    onStateChange ? onStateChange() : '';
  }

  req.onreadystatechange = function(e) {
    if (this.readyState == 4 && this.status == 200) {
      text = '';
      if (this.responseText !== '') {
        try {
          text = JSON.parse(this.responseText);
        } catch (e) {
          if (debug) { console.error(this.responseText); };
        }
      }
      done ? done(text) : '';
    }
  }
  req.onerror = function(e) {
    if (debug) { console.error('error:' + e); };
  }
  req.send(data);

}