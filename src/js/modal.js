var modals = (function() {
  var modals = [];

  function Modal(modal) {
    var self = this;
    this.modalWindow = modal;
    this.header = this.modalWindow.querySelector('.smf__header');
    this.content = this.modalWindow.querySelector('.smf__content');
    this.closeBtn = this.content.appendChild(createCloseBtn());

    this.modalWindow.addEventListener('click', function(e) {
      if (e.target === this || e.srcElement === this) {
        self.close(e);
      }
    });

    this.closeBtn.addEventListener('click', function(e) { self.close(e) });
  }

  Modal.prototype = {
    'close': function(e) {
      e.preventDefault();
      var self = this;

      self.modalWindow.classList.add('smf__hide');
      self.content.classList.add('smf__toTop');

      setTimeout(function() {
        document.body.classList.remove('smf__modal_open');
        document.body.style.paddingRight = 0;
        self.modalWindow.style.display = 'none';
        self.modalWindow.classList.remove('smf__hide');
        self.content.classList.remove('smf__toTop');
      }, 300);
    },

    'hideContent': function() {
      for (var i = 0; i < this.content.children.length; i++) {
        this.content.children[i].classList.add('smf__zero_height');
      }
    }

  }

  function initModals(selector) {
    var links = document.querySelectorAll('a[data-target]');
    var allModals = document.querySelectorAll(selector);

    for (var i = 0; i < allModals.length; i++) {
      modals.push(new Modal(allModals[i]));
    }

    for (var i = 0; i < links.length; i++) {
      links[i].addEventListener('click', function(e) {
        e.preventDefault();
        var target = this.getAttribute('data-target');
        var targetModal = document.getElementById(target);
        if (!targetModal) return false;
        document.body.style.paddingRight = getScrollWidth() + 'px';
        document.body.classList.add('smf__modal_open');
        targetModal.style.display = 'block';
      });
    }
    return modals;
  }

  function getScrollWidth() {
    if (window.innerHeight < document.body.scrollHeight) {
    return window.innerWidth - document.body.clientWidth;
    } else {
      return 0;
    }
  }

  function createCloseBtn() {
    var a = document.createElement('a');
    a.href = '#';
    a.className = 'smf__close_link';
    var icon = document.createElement('span');
    icon.className = 'smf__close';
    icon.innerHTML = '&nbsp;';
    a.appendChild(icon);
    return a;
  }

  return {
    init: initModals,
    modals: modals
  }

})();