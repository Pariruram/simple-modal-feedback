<?php

@include_once("./sendEmail.php");

$emailConfig = include('./sendEmailConfig.php');
$formConfig = include("./formConfig.php");
$validateConfig = include("./validateConfig.php");

if (!isset($_POST)) return;

$errors = array();

if (isset($_POST['check']) && isset($_POST['formName'])) {
  validate($formConfig[$_POST['formName']], $validateConfig);
}

if (isset($_POST['submit']) && isset($_POST['formName']) && empty($_POST['as'])) {
  $data = validate($formConfig[$_POST['formName']], $validateConfig);
  if ($data) {
    sendEmail($emailConfig['email'],
              $emailConfig['from'],
              $formConfig[$_POST['formName']]['config']['title'],
              $formConfig[$_POST['formName']]['config']['subject'],
              $data);
  }
}

function validate($form, $validateConfig) {
  
  $errors = array();
  $data = array();

  foreach ($form['fields'] as $name => $field) {
    $rawData = isset($_POST[$name]) ? trim($_POST[$name]) : '';
    if ($field['required'] && !$rawData) {
      $errors[$name] .= "Поле обязательно для заполнения.<br/>";
      continue;
    }
    if (!$rawData) continue;
    if (isset($validateConfig[$field['type']]['rules'])) {
      foreach ($validateConfig[$field['type']]['rules'] as $ruleName => $rule) {
        switch ($ruleName) {
          case 'minLength':
            if (mb_strlen($rawData,'UTF-8') < $rule) {
                $errors[$name] .= $validateConfig[$field['type']]['errors'][$ruleName];
            }
            break;
          case 'preg':
            if (!preg_match($rule,$rawData)) {
                $errors[$name] .= $validateConfig[$field['type']]['errors'][$ruleName];
            }
            break;
        }
      }
    }
    $data[$name]['title'] = $field['title'];
    $data[$name]['value'] = $rawData;
  }

  if (count($errors) != 0) {
    echo json_encode($errors);
    return false;
  }
  return $data;
}


?>