<?php

return array( 
  'form-1' => array(
    'fields' => array(
      'name' => array(
        'title' => 'Имя',
        'type' => 'name',
        'required' => true
      ),
      'tel' => array(
        'title' => 'Телефон',
        'type' => 'tel'
      ),
      'msg' => array(
        'title' => 'Сообщение'
      )
    ),
    'config' => array(
      'subject' => 'Сообщение из формы обратной связи',
      'title' => 'Обратная связь'
    )
),
'form-2' => array(
    'fields' => array(
      'name' => array(
        'title' => 'Имя',
        'type' => 'name',
        'required' => true        
      ),
      'tel' => array(
        'title' => 'Телефон',
        'type' => 'tel'
      )
    ),
    'config' => array(
      'subject' => 'Сообщение из формы обратного звонка',
      'title' => 'Обратный звонок'
    )
),
'form-3' => array(
    'fields' => array(
      'name' => array(
        'title' => 'Имя',
        'type' => 'name'
      ),
      'tel' => array(
        'title' => 'Телефон',
        'type' => 'tel',
        'required' => true        
      )
    ),
    'config' => array(
      'subject' => 'Сообщение из формы обратного звонка',
      'title' => 'Обратный звонок'
    )
));

?>