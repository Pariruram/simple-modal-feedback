<?php
return array(
  'name' => array(
    'rules' => array(
      'minLength' => '3'
    ),
    'errors' => array(
      'minLength' => 'Минимальная длина поля 3 символа.'
    )
  ), 
  'email' => array(
    'rules' => array(
      'preg' => '%@%is',
    ),
    'errors' => array(
      'preg' => 'Возможно, поле содержит ошибку.'
    )
  ),
  'tel' => array(
    'rules' => array(
      'preg' => "/^((8|\+)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{5,10}$/"
    ),
    'errors' => array(
      'preg' => 'Возможно, поле содержит ошибку.'
    )
  )
);
?>