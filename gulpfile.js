var gulp = require('gulp');
var path = require('path');
var less = require('gulp-less');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var shorthand = require('gulp-shorthand');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('js', function () {
  return gulp.src([
    './src/js/init.js',
    './src/js/form.js',
    './src/js/modal.js',
  ])
    .pipe(concat('script.js'))
    .pipe(gulp.dest('./dist/js/'));
});

gulp.task('less', function () {
  return gulp.src('./src/css/*.less')
    .pipe(less({
      paths: [path.join(__dirname, 'less', 'includes')]
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(shorthand())
    .pipe(gulp.dest('./dist/css/'));
});

gulp.task('less-min', function () {
  return gulp.src('./src/css/*.less')
    .pipe(sourcemaps.init())
    .pipe(less({
      paths: [path.join(__dirname, 'less', 'includes')]
    }))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(shorthand())
    .pipe(cleanCSS({compatibility: 'ie9'}))
    .pipe(rename(function (path) {
     path.basename += ".min";
     }))
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('./dist/css/'));
});

gulp.task('move-php', function() {
  return gulp.src('./src/*.php')
  .pipe(gulp.dest('./dist/'));
})

gulp.task('move-html', function() {
  return gulp.src('./src/index-build.html')
  .pipe(rename(function(path){
    path.basename = 'index';
  }))
  .pipe(gulp.dest('./dist/'));
})

gulp.task('build', ['less','js','move-php','move-html']);
gulp.task('build-min', ['less-min','js','move-php','move-html']);